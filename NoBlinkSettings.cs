﻿using MelonLoader;

namespace NoBlink {
    public static class NoBlinkSettings {
        private static MelonPreferences_Category _category = MelonPreferences.CreateCategory(BuildInfo.Name, "No Blink");
        internal static MelonPreferences_Entry<bool> AggresiveMode = _category.CreateEntry("AggressiveMode", false, "Experimental Aggressive Avatar Patching", "Patches avatar animations, blendshapes and weightpainting to futher prevent blinking, if you find any avatar that still blinks with this enabled please open an issue.");
    }
}