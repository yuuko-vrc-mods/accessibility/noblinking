﻿using System;
using System.Collections.Generic;
using System.Linq;
using MelonLoader;
using RealisticEyeMovements;
using UnhollowerBaseLib;
using UnityEngine;
using VRC.SDK3.Avatars.Components;

namespace NoBlink {
    public static class AggressiveMode {
        public static void Init() {
            NoBlinkSettings.AggresiveMode.OnValueChanged += (old, newOne) => Toggle(newOne); //todo: Reload all avatars on value change
            SetupEyeLookSettingsSDK3Hook.Install();
            Toggle(NoBlinkSettings.AggresiveMode.Value);
        }

        private static void Toggle(bool shouldEnable) {
            if (shouldEnable) {
                MelonLogger.Warning("Enabling aggressive avatar patching");
                SetupEyeLookSettingsSDK3Hook.onSetup += PreventBlinking;
            } else {
                MelonLogger.Warning("Disabling aggressive avatar patching");
                SetupEyeLookSettingsSDK3Hook.onSetup -= PreventBlinking;
            }
        }

        private static void PreventBlinking(EyeLookController eyeLookController) {
            EyeAndHeadAnimator eyeAndHeadAnimator = eyeLookController.field_Private_EyeAndHeadAnimator_0;
            var wearerName = eyeAndHeadAnimator.transform.parent?.parent?.GetComponent<VRCPlayer>()?.prop_Player_0?.prop_APIUser_0?.displayName ?? "A Player";
            try {
                switch (eyeAndHeadAnimator.controlData.eyelidControl) {
                    case ControlData.EyelidControl.Blendshapes: //Uses blendshapes for blinking
                        PatchBlendShapes(eyeAndHeadAnimator.controlData.blendshapesForBlinking?.ToArray());
                        break;
                    case ControlData.EyelidControl.Bones: //Uses bones for blinking
                        PatchBones(eyeAndHeadAnimator.controlData, eyeAndHeadAnimator.GetComponentsInChildren<SkinnedMeshRenderer>());
                        break;
                    default: //Most likely some custom av3 setup, try to patch the controller
                        //todo: find all blink animations / parameters in avatar
                        throw new Exception("Avatar uses a currently unsupported method of blinking, or can't blink already");
                        break;
                }
            } catch (Exception e) {
                MelonLogger.Msg($"Failed to aggressively patch {wearerName}'s avatar: " + e.Message);
            }
        }

        private static void PatchBones(ControlData controlData, Il2CppArrayBase<SkinnedMeshRenderer> skinnedMeshes) {
            List<Transform> bonesToFilter = new List<Transform>();

            #region Get a clean list of the bones to filter away

            bonesToFilter.Add(controlData.lowerEyeLidLeft);
            bonesToFilter.Add(controlData.upperEyeLidLeft);
            bonesToFilter.Add(controlData.lowerEyeLidRight);
            bonesToFilter.Add(controlData.upperEyeLidRight);
            bonesToFilter = bonesToFilter.Where(bone => bone != null).SelectMany(s => s.GetComponentsInChildren<Transform>(true).Append(s)).Distinct().ToList();

            #endregion

            #region Set the weight of all eyelid bones to 0

            foreach (var skinnedMesh in skinnedMeshes) {
                var indexesForMesh = bonesToFilter.Select(s => skinnedMesh.bones.IndexOf(s)).ToList();
                var boneWeights = skinnedMesh.sharedMesh.boneWeights;

                for (var boneWeightIndex = 0; boneWeightIndex < boneWeights.Count; boneWeightIndex++) {
                    BoneWeight weight = boneWeights[boneWeightIndex];
                    if (indexesForMesh.Contains(weight.boneIndex0))
                        weight.weight0 = 0f;
                    if (indexesForMesh.Contains(weight.boneIndex1))
                        weight.weight1 = 0f;
                    if (indexesForMesh.Contains(weight.boneIndex2))
                        weight.weight2 = 0f;
                    if (indexesForMesh.Contains(weight.boneIndex3))
                        weight.weight3 = 0f;
                    boneWeights[boneWeightIndex] = weight;
                }
            }

            #endregion
        }

        private static void PatchBlendShapes(ControlData.EyelidPositionBlendshape[] blendShapes) {
            try {
                foreach (var eyelidPositionBlendshape in blendShapes) {
                    Mesh mesh = eyelidPositionBlendshape.skinnedMeshRenderer.sharedMesh;
                    List<int> indexesOfVertsToPatch = new List<int>();

                    #region Get a list of verticies to filter from blendshapes

                    var frameCount = mesh.GetBlendShapeFrameCount(eyelidPositionBlendshape.index);
                    Il2CppStructArray<Vector3> frameVerticies = new Il2CppStructArray<Vector3>(mesh.vertexCount);
                    for (int i = 0; i < frameCount; i++) {
                        try {
                            mesh.GetBlendShapeFrameVertices(eyelidPositionBlendshape.index, i, frameVerticies, null, null);
                            indexesOfVertsToPatch.AddRange(frameVerticies.IndexesOf(candidate => candidate != Vector3.zero));
                        } catch (Exception e) {
                            MelonLogger.Msg("Error handling frame: " + i + "," + e.Message);
                        }
                    }

                    indexesOfVertsToPatch = indexesOfVertsToPatch.Distinct().ToList();

                    #endregion

                    //We need to now make a copy of all the blendshapes first as unity doesnt allow us to edit existing blendshape frames
                    //Therefore we copy, filter and reconstruct all blendshapes
                    var blendShapesCopy = CopyBlendShapes(mesh);
                    mesh.ClearBlendShapes(); //Remove all blendshapes
                    FilterBlendShapes(blendShapesCopy, indexesOfVertsToPatch); //Filter our new blendshapes
                    ConstructBlendShapes(mesh, blendShapesCopy); //Construct the new blendshapes
                }
            } catch (Exception e) {
                MelonLogger.Msg("Exception in nuteralizing blendshapes: " + e.Message);
            }
        }

        #region Utility functions for Copying, Filtering and Constructing blendshapes

        private static void ConstructBlendShapes(Mesh mesh, List<BlendShapeData> blendShapeDatas) {
            foreach (var blendShapeData in blendShapeDatas) {
                for (int frameIndex = 0; frameIndex < blendShapeData.FrameCount; frameIndex++) {
                    try {
                        mesh.AddBlendShapeFrame(blendShapeData.Name, blendShapeData.FrameWeights[frameIndex], blendShapeData.FrameVerticies[frameIndex], blendShapeData.FrameNormals[frameIndex], blendShapeData.FrameTangents[frameIndex]);
                    } catch (Exception e) {
                        MelonLogger.Msg($"{frameIndex}: {e.Message}");
                    }
                }
            }
        }

        private static void FilterBlendShapes(List<BlendShapeData> blendShapesCopy, List<int> vertIndexesToNuteralize) {
            foreach (var blendShapeData in blendShapesCopy) {
                for (int frameIndex = 0; frameIndex < blendShapeData.FrameCount; frameIndex++) {
                    foreach (var i1 in vertIndexesToNuteralize) {
                        blendShapeData.FrameVerticies[frameIndex][i1] = Vector3.zero;
                    }
                }
            }
        }

        private struct BlendShapeData {
            public string Name;
            public int FrameCount;
            public float[] FrameWeights;
            public Il2CppStructArray<Vector3>[] FrameVerticies;
            public Il2CppStructArray<Vector3>[] FrameNormals;
            public Il2CppStructArray<Vector3>[] FrameTangents;
        }

        private static List<BlendShapeData> CopyBlendShapes(Mesh mesh) {
            List<BlendShapeData> shapeData = new List<BlendShapeData>();
            for (int blendShapeIndex = 0; blendShapeIndex < mesh.blendShapeCount; blendShapeIndex++) {
                var blendCopy = new BlendShapeData();
                blendCopy.Name = mesh.GetBlendShapeName(blendShapeIndex);
                blendCopy.FrameCount = mesh.GetBlendShapeFrameCount(blendShapeIndex);
                blendCopy.FrameNormals = new Il2CppStructArray<Vector3>[blendCopy.FrameCount];
                blendCopy.FrameVerticies = new Il2CppStructArray<Vector3>[blendCopy.FrameCount];
                blendCopy.FrameTangents = new Il2CppStructArray<Vector3>[blendCopy.FrameCount];
                blendCopy.FrameWeights = new float[blendCopy.FrameCount];
                for (int frameIndex = 0; frameIndex < blendCopy.FrameCount; frameIndex++) {
                    blendCopy.FrameVerticies[frameIndex] = new Il2CppStructArray<Vector3>(mesh.vertexCount);
                    blendCopy.FrameNormals[frameIndex] = new Il2CppStructArray<Vector3>(mesh.vertexCount);
                    blendCopy.FrameTangents[frameIndex] = new Il2CppStructArray<Vector3>(mesh.vertexCount);
                    mesh.GetBlendShapeFrameVertices(blendShapeIndex, frameIndex, blendCopy.FrameVerticies[frameIndex], blendCopy.FrameNormals[frameIndex], blendCopy.FrameTangents[frameIndex]);
                    blendCopy.FrameWeights[frameIndex] = mesh.GetBlendShapeFrameWeight(blendShapeIndex, frameIndex);
                }

                shapeData.Add(blendCopy);
            }

            return shapeData;
        }

        #endregion
    }
}