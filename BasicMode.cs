﻿using System.Linq;
using System.Reflection;
using HarmonyLib;
using MelonLoader;
using RealisticEyeMovements;
using UnhollowerRuntimeLib.XrefScans;

namespace NoBlink {
    public static class BasicMode {
        //Find methods responsible for blinking
        private static MethodBase _lateUpdate = typeof(EyeAndHeadAnimator).GetMethod(nameof(EyeAndHeadAnimator.LateUpdate));
        private static MethodBase _veryLateUpdate = XrefScanner.XrefScan(_lateUpdate).LastOrDefault(x => x.TryResolve() != null && x.TryResolve().DeclaringType == typeof(EyeAndHeadAnimator)).TryResolve();
        private static MethodBase _updateBlinking = XrefScanner.XrefScan(_veryLateUpdate).LastOrDefault(x => x.TryResolve() != null && x.TryResolve().DeclaringType == typeof(EyeAndHeadAnimator)).TryResolve();

        public static void Init(HarmonyLib.Harmony harmonyInstance) {
            HarmonyMethod noBlinkHook = new HarmonyMethod(typeof(BasicMode).GetMethod(nameof(UpdateBlinkingPrefix)));
            harmonyInstance.Patch(_updateBlinking, noBlinkHook);
            MelonLogger.Msg("Patched blinking");
        }

        public static bool UpdateBlinkingPrefix() => false;
    }
}