﻿using System.Reflection;
using MelonLoader;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle(NoBlink.BuildInfo.Name)]
[assembly: AssemblyDescription(NoBlink.BuildInfo.Description)]
[assembly: AssemblyCompany(NoBlink.BuildInfo.Company)]
[assembly: AssemblyProduct(NoBlink.BuildInfo.Name)]
[assembly: AssemblyCopyright("Copyright © "+NoBlink.BuildInfo.Author+" 2021")]
[assembly: AssemblyTrademark(NoBlink.BuildInfo.Company)]

[assembly: AssemblyVersion(NoBlink.BuildInfo.Version)]
[assembly: AssemblyFileVersion(NoBlink.BuildInfo.Version)]

[assembly: MelonInfo(typeof(NoBlink.NoBlinkMod), NoBlink.BuildInfo.Name, NoBlink.BuildInfo.Version, NoBlink.BuildInfo.Author, NoBlink.BuildInfo.DownloadLink)]
[assembly: MelonColor(NoBlink.BuildInfo.ConsoleColor)]
[assembly: MelonGame(NoBlink.BuildInfo.GameDeveloper, NoBlink.BuildInfo.Game)]