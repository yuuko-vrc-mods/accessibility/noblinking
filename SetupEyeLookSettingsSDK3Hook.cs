﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using HarmonyLib;
using MelonLoader;
using RealisticEyeMovements;
using UnhollowerBaseLib;
using UnhollowerRuntimeLib.XrefScans;
using UnityEngine;
using VRC.SDK3.Avatars.Components;
using Harmony = HarmonyLib.Harmony;

namespace NoBlink {
    public static class SetupEyeLookSettingsSDK3Hook {
        public static Action<EyeLookController> onSetup;

        #region Candidiate Reflection

        //private non-static void (CustomEyeLookSettings)
        private static IEnumerable<MethodInfo> _setupEyeLookSettingsSDK3Candidates = typeof(EyeLookController).GetMethods().Where(candidate =>
            !candidate.IsStatic &&
            candidate.GetParameters().Length == 1 &&
            candidate.Name.Contains("Private") &&
            candidate.GetParameters().First().ParameterType == typeof(VRCAvatarDescriptor.CustomEyeLookSettings) &&
            candidate.Name.Contains("Method")
        );

        #endregion
        
        private delegate void OriginalSetupEyeLookSettingsSDK3(IntPtr instance, IntPtr settings);
        private static OriginalSetupEyeLookSettingsSDK3 _originalSetupEyeLookSettingsSDK3;
        
        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        private static IntPtr _hookPtr = typeof(SetupEyeLookSettingsSDK3Hook).GetMethod(nameof(PostHook), BindingFlags.NonPublic | BindingFlags.Static).MethodHandle.GetFunctionPointer();

        public static unsafe void Install() {
            MethodInfo setupEyeLookSettingsSDK3 = _setupEyeLookSettingsSDK3Candidates.Single();
            var methodPtr = *(IntPtr*) (IntPtr) UnhollowerUtils.GetIl2CppMethodInfoPointerFieldForGeneratedMethod(setupEyeLookSettingsSDK3).GetValue(null);
            MelonUtils.NativeHookAttach((IntPtr) (&methodPtr), _hookPtr);
            _originalSetupEyeLookSettingsSDK3 = Marshal.GetDelegateForFunctionPointer<OriginalSetupEyeLookSettingsSDK3>(methodPtr);
        }


        #region Post Hook
        [SuppressMessage("ReSharper", "Unity.IncorrectMonoBehaviourInstantiation")]
        private static void PostHook(IntPtr instancePtr, IntPtr settings) {
            _originalSetupEyeLookSettingsSDK3(instancePtr, settings);
            EyeLookController x = new EyeLookController(instancePtr);
            onSetup?.Invoke(x);
        }
        #endregion
    }
}