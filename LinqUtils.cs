﻿using System;
using System.Collections.Generic;
using UnhollowerBaseLib;

namespace NoBlink {
    public static class LinqUtils {
        public static List<int> IndexesOf<T>(this Il2CppStructArray<T> array, Func<T, bool> predicate) where T : unmanaged {
            List<int> indexes = new List<int>();
            for (int i = 0; i < array.Count; i++)
                if (predicate(array[i]))
                    indexes.Add(i);
            return indexes;
        }
    }
}