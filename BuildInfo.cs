﻿using System;
using System.Collections.Generic;
using MelonLoader;

namespace NoBlink {
    public static class BuildInfo {
        public const string Author = "Yuuko Shionji";
        public const string Name = "NoBlink";
        public const string Version = "1.0.0";
        public const string Company = null;
        public const string Description = "Makes the VRChat UI Tacticle";
        public const string DownloadLink = null;

        public const ConsoleColor ConsoleColor = System.ConsoleColor.Cyan;
        public const string GameDeveloper = "VRChat";
        public const string Game = "VRChat";
    }
}