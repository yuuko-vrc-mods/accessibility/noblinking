﻿using System.Collections.Generic;
using System.Linq;
using MelonLoader;
using RealisticEyeMovements;
using UnhollowerBaseLib;
using UnityEngine;
using VRC.SDK3.Avatars.Components;
using Exception = System.Exception;

namespace NoBlink {
    public class NoBlinkMod : MelonMod {
        public override void OnApplicationStart() {
            try {
                BasicMode.Init(HarmonyInstance);
            } catch (Exception e) {
                MelonLogger.Error("Failed to initialize basic patch, You are likely to see avatars blink!");
                MelonLogger.Error(e);
            }
            
            try {
                AggressiveMode.Init();
            } catch (Exception e) {
                MelonLogger.Error("Failed to initialize aggressive avatar patching, You might see some avatars blink!");
                MelonLogger.Error(e);
            }
        }
    }
}